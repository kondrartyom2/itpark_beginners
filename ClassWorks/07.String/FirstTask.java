import java.util.*;

class FirstTask {
	public static void main(String[] args) {
		String str = new String("Hello");
		String str2 = "Hello";
		System.out.println(str == str2);
		System.out.println(str.equals(str2));
		System.out.println("Введите длинну массива");
		Scanner s = new Scanner(System.in);
		Random random = new Random();
		int[][] array = new int[s.nextInt()][];
		for (int i = 0; i < array.length; i++) {
			array[i] = new int[i + 1];
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] =  random.nextInt(40);
			}
		}
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
	}
}