import java.util.Scanner;

public class CW3 {
    public static void main(String[] args) {
        // TODO: 19.03.2021 1 задание
//        Scanner s = new Scanner(System.in);
//        System.out.println("Введите число от 0 до 9");
//        int number = s.nextInt();
//        number = number > 2 ? 4 : (number % 2) == 0 ? number / 2 : number;
//        System.out.println(number);
        // TODO: 19.03.2021 2 задание
        Scanner s = new Scanner(System.in);
        System.out.println("Введите число от 0 до 19");
        double number = s.nextDouble();
        if ((number * 10) % 10 == 0) {
            if ((number != 0) && (number > 6)) {
                System.out.println(number);
            } else {
                number += 6;
                System.out.println(number);
            }
        } else if (((number * 10) % 10) % 2 == 0) {
            System.out.println(number / 2);
        } else {
            System.out.println(number * 2);
        }
    }
}
