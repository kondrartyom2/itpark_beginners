import java.util.Random;
import java.util.Scanner;

public class RationFractions {

    private int numerator;
    private int denominator;

    public RationFractions() {
    }

    public RationFractions(int numerator, int denominator) {
        this.numerator = numerator;
        int number = numerator;
        this.denominator = denominator;
    }

    public String toString() {
        return numerator + "/" + denominator;
    }

    public int getDenominator() {
        return denominator;
    }

    public int getNumerator() {
        return numerator;
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    public void reduce() {
        int limit = Math.min(getDenominator(), getNumerator());
        for (int i = limit; i > 0; i++) {
            if (getNumerator() % i == 0 && getDenominator() % i == 0) {
                this.numerator = this.numerator / i;
                this.denominator = this.denominator / i;
            }
        }
    }

    public RationFractions mult(RationFractions rationFractionOne) {
        RationFractions rationFractionTwo = new RationFractions(this.numerator * rationFractionOne.getNumerator(),
                this.denominator * rationFractionOne.getDenominator());
        rationFractionTwo.reduce();
        return rationFractionTwo;
    }

    public void mult2(RationFractions rationFractionOne) {
        this.numerator *= rationFractionOne.numerator;
        this.denominator *= rationFractionOne.denominator;
    }

    public RationFractions div(RationFractions rationFractionOne) {
        RationFractions rationFractionTwo = new RationFractions(this.numerator * rationFractionOne.getDenominator(),
                this.denominator * rationFractionOne.getNumerator());
        rationFractionTwo.reduce();
        return rationFractionTwo;
    }

    public void div2(RationFractions rationFractionOne) {
        this.numerator *= rationFractionOne.getDenominator();
        this.denominator *= rationFractionOne.getNumerator();
    }

    public double value() {
        return this.numerator / this.denominator;
    }
}

