import java.util.Scanner;

public class Functions {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write a count of numbers");
        int[] array = getArray(scanner.nextInt(), scanner);
        System.out.println(toString(array));
        System.out.println(
                toString(
                        plusTwoArrays(plusTwo(array),
                                minusTwo(array)
                        )
                )
        );
    }

    public static int[] getArray(int arrayLength, Scanner scanner) {
        int[] array = new int[arrayLength];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        return array;
    }

    public static String toString(int[] array) {
        String answer = "(";
        for (int i = 0; i < array.length; i++) {
            answer += array[i] + ", ";
        }
        answer += "\b\b)";
        return answer;
    }

    public static int[] plusTwoArrays(int[] arrayOne, int[] arrayTwo) {
        int minLength = (arrayOne.length > arrayTwo.length) ? arrayTwo.length : arrayOne.length;
        for (int i = 0; i < minLength; i++) {
            arrayOne[i] += arrayTwo[i];
        }
        return arrayOne;
    }

    public static int[] multTwo(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] *= 2;
        }
        return array;
    }

    public static int[] minusTwo(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] -= 2;
        }
        return array;
    }

    public static int[] plusTwo(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] += 2;
        }
        return array;
    }
}
