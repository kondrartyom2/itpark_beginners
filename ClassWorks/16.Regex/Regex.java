import java.nio.file.Path;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Pattern pattern = Pattern.compile("^(\\+7|8)\\d{10}$"); //регулярка для мобилы
        System.out.println(pattern.pattern());
        System.out.println("Введите номер телефона");
        System.out.println(Pattern.matches(pattern.pattern(), scanner.nextLine()));
        Pattern patternOne = Pattern.compile("(8843)?\\d{7}");
        System.out.println("Введите текст где надо найти номер телефона");
        Matcher matcherOne = patternOne.matcher(scanner.nextLine());
        while (matcherOne.find()) {
            System.out.println("да!");
            System.out.println(matcherOne.group());
        }

    }
}
