public class User {
    private String name;
    private String surName;
    private int age;
    private int size;
    private long id;

    public User() {
    }

    public User(String name, String surName, int age, int size) {
        this.name = name;
        this.surName = surName;
        this.age = age;
        this.size = size;
    }

    public User(String name, String surName) {
        this.name = name;
        this.surName = surName;
    }

    public String getName() {
        return this.name;
    }

    public String getSurName() {
        return this.surName;
    }

    public int getAge() {
        return this.age;
    }

    public int getSize() {
        return this.size;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSize(int size) {
        this.size = size;
    }

    private int plusOne(int elem) {
        return elem + 1;
    }

    public void addYear() {
        this.age = plusOne(this.age);
    }

    public String toString() {
        return this.name + " " + this.surName + " " + this.age;
    }

    public boolean equals(User user) {
        return (
                this.name.equals(user.getName()) & this.surName.equals(user.getSurName()) & this.age == user.getAge()
        ) ? true : false;
    }
}
