public class Main {
    public static void main(String[] args) {
        User user = new User("Artem", "Kondratiev", 20, 186);
        User user2 = new User("Artem", "Kondratiev", 20, 186);
        System.out.println(user.equals(user2));

        System.out.println(user.toString());
        user.setName("Саид");
        user.setSurName("Файзуллин");
        System.out.println(user.toString());
        user.addYear();
    }
}
