import java.util.Scanner;

public class Game {

    public static int powerOfKick(Scanner scanner) {
        System.out.println("Введите силу удара от 1 до 9");
        int power = scanner.nextInt();
        while (power > 10 | power < 0) {
            System.out.println("Введите силу удара от 1 до 9");
            power = scanner.nextInt();
        }
        return power;
    }

    public static boolean move(Player player, Player player2, Scanner scanner) {
        if (player.isAlive()) {
            System.out.println("Ходит - " + player.toString());
            player2.kicked(powerOfKick(scanner));
        } else {
            System.out.println("Выиграл - " + player2.getUserName());
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите ваше имя (1)");
        Player player = new Player(scanner.nextLine());
        System.out.println("Сохранен");
        System.out.println("Введите ваше имя (2)");
        Player player2 = new Player(scanner.nextLine());
        System.out.println("Сохранен");
        boolean flag = true;
        while (true) {
            if (flag) {
                if (move(player, player2, scanner)) {
                    break;
                }
                flag = false;
            } else {
                if (move(player2, player, scanner)) {
                    break;
                }
                flag = true;
            }
        }
    }
}
