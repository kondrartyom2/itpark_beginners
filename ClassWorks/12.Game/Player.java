import java.util.Random;

public class Player {
    private int hp;
    private String userName;

    public Player(String userName) {
        this.hp = 100;
        this.userName = userName;
    }

    public int getHp() {
        return hp;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void kicked(int power) {
        if ((power > 0) && (power < 10)) {
            Random r = new Random( );
            int ver = r.nextInt(100);
            if (power == 1) {
                this.hp -= power * 10;
            } else if ((power == 2) && (ver <= 89)) {
                this.hp -= power * 10;
            } else if ((power == 3) && (ver <= 78)) {
                this.hp -= power * 10;
            } else if ((power == 4) && (ver <= 67)) {
                this.hp -= power * 10;
            } else if ((power == 5) && (ver <= 56)) {
                this.hp -= power * 10;
            } else if ((power == 6) && (ver <= 45)) {
                this.hp -= power * 10;
            } else if ((power == 7) && (ver <= 34)) {
                this.hp -= power * 10;
            } else if ((power == 8) && (ver <= 23)) {
                this.hp -= power * 10;
            } else if ((power == 9) && (ver <= 12)) {
                this.hp -= power * 10;
            }
        }
    }

    public boolean isAlive() {
        return this.hp > 0;
    }

    public String toString() {
        return this.userName + "\n" + this.hp;
    }
}
