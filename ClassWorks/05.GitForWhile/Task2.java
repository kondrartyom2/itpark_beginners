import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        System.out.println("Write a number");
        Scanner s = new Scanner(System.in);
        int side = s.nextInt();
        int space = side - 1;
        for (int i = 0; i < side; i++) {

            for(int k = 0; k < space; k++) {
                System.out.print(" ");
            }
            space--;

            for (int j = 0; j < side; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}
