import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        System.out.println("Write a number");
        Scanner s = new Scanner(System.in);
        int side = s.nextInt();
        for (int i = 0; i < side; i++) {
            for (int j = 0; j < side; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}
