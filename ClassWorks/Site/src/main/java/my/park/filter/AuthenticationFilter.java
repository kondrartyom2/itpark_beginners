package my.park.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthenticationFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession(false);

        Boolean isAuthenticated = false;

        Boolean sessionExist = session != null;

        Boolean isCss = request.getRequestURI().endsWith(".css");

        Boolean isJs = request.getRequestURI().endsWith(".js");

        Boolean isImage = request.getRequestURI().endsWith(".jpg") || request.getRequestURI().endsWith(".png")
                || request.getRequestURI().endsWith(".jpeg");

        Boolean isRequestOnStatics = isCss || isImage || isJs;

        Boolean signInPage = request.getRequestURI().equals("/signin");

        Boolean signUpPage = request.getRequestURI().equals("/signup");

        Boolean startPage = request.getRequestURI().equals("/");


        Boolean isRequestOnOpenPage = signInPage || signUpPage || startPage;


        if (sessionExist) {
            isAuthenticated = session.getAttribute("userDto") != null;
        }

        if (isRequestOnStatics) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else if ((isAuthenticated && !isRequestOnOpenPage) || (!isAuthenticated && isRequestOnOpenPage)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else if (isAuthenticated) {
            response.sendRedirect("/home");
        } else {
            response.sendRedirect("/signin");
        }
    }

    @Override
    public void destroy() {

    }
}
