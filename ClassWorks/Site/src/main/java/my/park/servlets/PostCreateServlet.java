package my.park.servlets;

import my.park.dto.UserDto;
import my.park.forms.PostForm;
import my.park.services.impl.PostServiceImpl;
import my.park.services.iterface.PostService;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/profile/add_post")
public class PostCreateServlet extends HttpServlet {
    private ApplicationContext applicationContext;

    private ServletContext contextListener;

    private PostService postService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.contextListener = config.getServletContext();
        this.applicationContext = (ApplicationContext) contextListener.getAttribute("applicationContext");
        this.postService = (PostServiceImpl) applicationContext.getBean(PostService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request , HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/jsp/post_create_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request , HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);

        UserDto user = (UserDto) request.getSession(true).getAttribute("userDto");

        PostForm post = PostForm.builder()
                .title(request.getParameter("title"))
                .text(request.getParameter("text"))
                .userId(user.getId())
                .build();

        postService.addPost(post);

        response.sendRedirect("/profile");
    }
}
