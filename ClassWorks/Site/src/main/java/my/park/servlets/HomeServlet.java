package my.park.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import my.park.forms.PostForm;
import my.park.services.impl.PostServiceImpl;
import my.park.services.iterface.PostService;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/home")
public class HomeServlet extends HttpServlet {

    private ApplicationContext applicationContext;

    private ServletContext contextListener;

    private PostService postService;

    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.contextListener = config.getServletContext();
        this.applicationContext = (ApplicationContext) contextListener.getAttribute("applicationContext");
        this.postService = (PostServiceImpl) applicationContext.getBean(PostService.class);
        this.objectMapper = (ObjectMapper) applicationContext.getBean(ObjectMapper.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("posts", postService.getAllPosts());
        request.getRequestDispatcher("/WEB-INF/jsp/home_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PostForm post = objectMapper.readValue(request.getReader(), PostForm.class);

        String posts;

        if (post.getText().length() != 0){
            posts = objectMapper.writeValueAsString(postService.getPostsByText(post.getText()));
        } else {
            posts = objectMapper.writeValueAsString(postService.getAllPosts());
        }

        response.setContentType("application/json");
        response.getWriter().write(posts);
    }
}
