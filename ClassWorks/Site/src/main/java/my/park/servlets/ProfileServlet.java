package my.park.servlets;

import my.park.dto.PostDto;
import my.park.dto.UserDto;
import my.park.services.impl.PostServiceImpl;
import my.park.services.iterface.PostService;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {

    private ApplicationContext applicationContext;

    private ServletContext contextListener;

    private PostService postService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.contextListener = config.getServletContext();
        this.applicationContext = (ApplicationContext) contextListener.getAttribute("applicationContext");
        this.postService = (PostServiceImpl) applicationContext.getBean(PostService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession(true);

        UserDto userDto = (UserDto) session.getAttribute("userDto");

        List<PostDto> posts = postService.getAllPostsByUserId(userDto.getId());

        request.setAttribute("posts", posts);

        request.getRequestDispatcher("/WEB-INF/jsp/profile_page.jsp").forward(request, response);
    }
}
