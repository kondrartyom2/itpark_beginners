package my.park.servlets;

import my.park.services.impl.UserServiceImpl;
import my.park.services.iterface.UserService;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/users")
public class UserServlet extends HttpServlet {

    private ApplicationContext applicationContext;

    private ServletContext contextListener;

    private UserService userService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.contextListener = config.getServletContext();
        this.applicationContext = (ApplicationContext) contextListener.getAttribute("applicationContext");
        this.userService = (UserServiceImpl) applicationContext.getBean(UserService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("users", userService.getAllUsers());
        request.getRequestDispatcher("/WEB-INF/jsp/users_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        userService.addUser(request.getParameter("firstName"), request.getParameter("email"));
        response.sendRedirect("/users");
    }
}
