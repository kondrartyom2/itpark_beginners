package my.park.servlets;

import my.park.dto.UserDto;
import my.park.services.impl.SignInServiceImpl;
import my.park.services.impl.UserServiceImpl;
import my.park.services.iterface.SignInService;
import my.park.services.iterface.UserService;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/signin")
public class SignInServlet extends HttpServlet {
    private ApplicationContext applicationContext;

    private ServletContext contextListener;

    private SignInService signInService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.contextListener = config.getServletContext();
        this.applicationContext = (ApplicationContext) contextListener.getAttribute("applicationContext");
        this.signInService = (SignInServiceImpl) applicationContext.getBean(SignInService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/jsp/signin_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDto userDto = signInService.signIn(
                UserDto.builder()
                        .email(request.getParameter("email"))
                        .hashPassword(request.getParameter("password"))
                        .build()
        );
        if (userDto != null) {
            HttpSession session = request.getSession(true);
            session.setAttribute("userDto", userDto);
            response.sendRedirect("/");
        } else {
            response.sendRedirect("/signup");
        }
    }

}
