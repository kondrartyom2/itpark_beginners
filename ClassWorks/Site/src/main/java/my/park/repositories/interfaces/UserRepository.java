package my.park.repositories.interfaces;

import my.park.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    List<User> getAllUsers();
    void addUser(User user);

    User save(User user);

    User getUserById(Long userId);

    List<String> checkEmails();

    Optional<User> findByEmail(String email);
}
