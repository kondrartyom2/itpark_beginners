package my.park.repositories.impl;

import my.park.models.Post;
import my.park.repositories.interfaces.PostRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class PostRepositoryImpl implements PostRepository {

    private DataSource dataSource;

    private JdbcTemplate jdbcTemplate;

    public PostRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private final RowMapper<Post> postRowMapper = (row, rowNumber) -> Post.builder()
            .id(row.getLong("id"))
            .userId(row.getLong("user_id"))
            .title(row.getString("title"))
            .text(row.getString("text"))
            .build();

    //language=SQL
    private static final String SQL_FIND_ALL = "select * from post;";

    //language=SQL
    private static final String SQL_FIND_ALL_BY_USER_ID = "select * from post where user_id = ?;";

    //language=SQL
    private static final String SQL_INSERT_POST = "insert into " +
            "post(user_id, title, text) " +
            "values (?, ?, ?) returning id;";

    //language=SQL
    private static final String SQL_FIND_BY_TEXT = "select * from post where text like ?;";

    @Override
    public Post save(Post post) {
        post.setId(
                jdbcTemplate.queryForObject(
                        SQL_INSERT_POST, Long.class,
                        post.getUserId(), post.getTitle(), post.getText()
                ));
        return post;
    }

    @Override
    public List<Post> getAllPostsByUserId(Long userId) {
        return jdbcTemplate.query(SQL_FIND_ALL_BY_USER_ID, postRowMapper, userId);
    }

    @Override
    public List<Post> getAllPosts() {
        return jdbcTemplate.query(SQL_FIND_ALL, postRowMapper);
    }

    @Override
    public List<Post> findByText(String text) {
        return jdbcTemplate.query(SQL_FIND_BY_TEXT, postRowMapper, "%" + text + "%");
    }

}


