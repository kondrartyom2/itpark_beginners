package my.park.repositories.interfaces;

import my.park.models.Post;

import java.util.List;

public interface PostRepository {

    Post save(Post post);

    List<Post> getAllPostsByUserId(Long userId);

    List<Post> getAllPosts();

    List<Post> findByText(String text);
}
