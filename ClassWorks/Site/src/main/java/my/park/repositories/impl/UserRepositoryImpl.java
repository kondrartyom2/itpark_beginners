package my.park.repositories.impl;

import my.park.models.User;
import my.park.repositories.interfaces.UserRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserRepositoryImpl implements UserRepository {

    private DataSource dataSource;

    private JdbcTemplate jdbcTemplate;

    private final RowMapper<User> userRowMapper = (row, rowNumber) -> User.builder()
            .id(row.getLong("id"))
            .firstName(row.getString("name"))
            .email(row.getString("email"))
            .hashPassword(row.getString("hash_pass"))
            .build();

    //language=SQL
    private static final String SQL_FIND_ALL = "select * from users;";

    //language=SQL
    private static final String SQL_INSERT_USER = "insert into " +
            "users(name, email, hash_pass) " +
            "values (?, ?, ?) returning id;";

    //language=SQL
    private static final String SQL_FIND_BY_EMAIL = "select * from users where email = ?;";

    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from users where id = ?;";

    public UserRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<User> getAllUsers() {
        return jdbcTemplate.query(SQL_FIND_ALL, userRowMapper);
    }

    @Override
    public void addUser(User user) {
        jdbcTemplate.update(SQL_INSERT_USER, user.getFirstName(), user.getEmail());
    }

    @Override
    public User save(User user) {
        user.setId(
                jdbcTemplate.queryForObject(
                        SQL_INSERT_USER, Long.class,
                        user.getFirstName(), user.getEmail(), user.getHashPassword()
                ));
        return user;
    }

    @Override
    public User getUserById(Long userId) {
        return jdbcTemplate.queryForObject(SQL_FIND_BY_ID, userRowMapper, userId);
    }

    @Override
    public List<String> checkEmails() {
        List<String> emails = new ArrayList<>();
        for (User user : jdbcTemplate.query(SQL_FIND_ALL, userRowMapper)) {
            emails.add(user.getEmail());
        }
        return emails;
    }

    @Override
    public Optional<User> findByEmail(String email) {
        User user;
        try {
            user = jdbcTemplate.queryForObject(SQL_FIND_BY_EMAIL, userRowMapper, email);
        } catch (EmptyResultDataAccessException e) {
            user = null;
        }
        return Optional.ofNullable(user);
    }
}
