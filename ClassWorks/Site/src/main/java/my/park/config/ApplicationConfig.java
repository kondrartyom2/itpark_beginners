package my.park.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import my.park.repositories.impl.PostRepositoryImpl;
import my.park.repositories.impl.UserRepositoryImpl;
import my.park.services.impl.PostServiceImpl;
import my.park.services.impl.SignInServiceImpl;
import my.park.services.impl.SignUpServiceImpl;
import my.park.services.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:db.properties")
@ComponentScan(basePackages = "my.park")
public class ApplicationConfig {

    @Autowired
    private Environment environment;

    @Bean
    public DataSource dataSource() {
        return new HikariDataSource(hikariConfig());
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UserRepositoryImpl usersRepository() {
        return new UserRepositoryImpl(dataSource());
    }

    @Bean
    public UserServiceImpl userService() {
        return new UserServiceImpl(usersRepository());
    }

    @Bean
    public PostRepositoryImpl postRepository() {
        return new PostRepositoryImpl(dataSource());
    }

    @Bean
    public PostServiceImpl postService() {
        return new PostServiceImpl(postRepository(), usersRepository());
    }

    @Bean
    public HikariConfig hikariConfig() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(environment.getProperty("db.jdbc.url"));
        hikariConfig.setUsername(environment.getProperty("db.jdbc.username"));
        hikariConfig.setPassword(environment.getProperty("db.jdbc.password"));
        hikariConfig.setDriverClassName(environment.getProperty("db.jdbc.driver-class-name"));
        hikariConfig.setMaximumPoolSize(Integer.parseInt(environment.getProperty("db.jdbc.hikari.max-poll-size")));
        return hikariConfig;
    }

    @Bean
    public SignUpServiceImpl signUpService() {
        return new SignUpServiceImpl(usersRepository(), passwordEncoder());
    }

    @Bean
    public SignInServiceImpl signInService() {
        return new SignInServiceImpl(usersRepository(), passwordEncoder());
    }
}

