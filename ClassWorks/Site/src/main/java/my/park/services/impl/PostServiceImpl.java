package my.park.services.impl;

import my.park.dto.PostDto;
import my.park.forms.PostForm;
import my.park.models.Post;
import my.park.repositories.interfaces.PostRepository;
import my.park.repositories.interfaces.UserRepository;
import my.park.services.iterface.PostService;

import java.util.List;

import static my.park.dto.PostDto.from;

public class PostServiceImpl implements PostService {

    private PostRepository postRepository;

    private UserRepository userRepository;

    public PostServiceImpl(PostRepository postRepository, UserRepository userRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void addPost(PostForm postForm) {
        postRepository.save(
                Post.builder()
                        .userId(postForm.getUserId())
                        .title(postForm.getTitle())
                        .text(postForm.getText())
                        .build()
        );
    }

    @Override
    public List<PostDto> getAllPostsByUserId(Long userId) {
        return from(postRepository.getAllPostsByUserId(userId));
    }

    @Override
    public List<PostDto> getAllPosts() {
        List<PostDto> posts = from(postRepository.getAllPosts());
        for (PostDto post : posts) {
            post.setPostCreator(userRepository.getUserById(post.getUserId()).getEmail().split("@")[0]);
        }
        return posts;
    }

    @Override
    public List<PostDto> getPostsByText(String text) {
        return from(postRepository.findByText(text));
    }
}
