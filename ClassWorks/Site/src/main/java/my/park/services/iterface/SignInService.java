package my.park.services.iterface;

import my.park.dto.UserDto;

public interface SignInService {
    UserDto signIn(UserDto userDto);
}
