package my.park.services.iterface;

import my.park.dto.UserDto;

import java.util.List;

public interface UserService {
    void addUser(String name, String email);
    List<UserDto> getAllUsers();
}
