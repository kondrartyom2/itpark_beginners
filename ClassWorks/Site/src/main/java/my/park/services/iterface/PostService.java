package my.park.services.iterface;

import my.park.dto.PostDto;
import my.park.forms.PostForm;

import java.util.List;

public interface PostService {
    void addPost(PostForm postForm);

    List<PostDto> getAllPostsByUserId(Long userId);

    List<PostDto> getAllPosts();

    List<PostDto> getPostsByText(String text);
}
