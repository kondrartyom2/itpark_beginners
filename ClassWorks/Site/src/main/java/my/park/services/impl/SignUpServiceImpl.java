package my.park.services.impl;

import my.park.dto.UserDto;
import my.park.models.User;
import my.park.repositories.interfaces.UserRepository;
import my.park.services.iterface.SignUpService;
import org.springframework.security.crypto.password.PasswordEncoder;

public class SignUpServiceImpl implements SignUpService {

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    public SignUpServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Boolean signUp(UserDto userDto) {
        if (!userRepository.checkEmails().contains(userDto.getEmail())) {
            userRepository.save(
                    User.builder()
                            .firstName(userDto.getFirstName())
                            .email(userDto.getEmail())
                            .hashPassword(passwordEncoder.encode(userDto.getHashPassword()))
                            .build()
            );
            return true;
        }
        return false;
    }
}
