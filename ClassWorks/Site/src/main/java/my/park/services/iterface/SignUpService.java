package my.park.services.iterface;

import my.park.dto.UserDto;

public interface SignUpService {
    Boolean signUp(UserDto userDto);
}
