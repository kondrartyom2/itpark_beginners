package my.park.services.impl;

import my.park.dto.UserDto;
import my.park.models.User;
import my.park.repositories.interfaces.UserRepository;
import my.park.services.iterface.SignInService;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

public class SignInServiceImpl implements SignInService {

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    public SignInServiceImpl (UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDto signIn(UserDto userDto) {
        Optional<User> userOptional = userRepository.findByEmail(userDto.getEmail());
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            if (passwordEncoder.matches(userDto.getHashPassword(), user.getHashPassword())) {
                return UserDto.from(user);
            } else {
                return null;
            }
        }
        return null;
    }
}
