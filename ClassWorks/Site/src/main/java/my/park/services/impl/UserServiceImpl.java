package my.park.services.impl;

import my.park.dto.UserDto;
import my.park.models.User;
import my.park.repositories.interfaces.UserRepository;
import my.park.services.iterface.UserService;

import java.util.List;

import static my.park.dto.UserDto.from;

public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void addUser(String name, String email) {
        userRepository.addUser(
                User.builder()
                        .email(email)
                        .firstName(name)
                        .build()
        );
    }

    @Override
    public List<UserDto> getAllUsers() {
        return from(userRepository.getAllUsers());
    }
}
