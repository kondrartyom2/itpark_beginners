<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="my.park.dto.UserDto" %><%--
  Created by IntelliJ IDEA.
  User: Artem Work
  Date: 13.11.2020
  Time: 23:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <title>VOLKSWAGEN.RU</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../static/images/logo.jpg" type="image/x-icon"/>
    <link rel="shortcut icon" href="../../static/images/logo.jpg" type="image/x-icon"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../static/css/buttons.css" type="text/css"/>
    <link rel="stylesheet" href="../../static/css/blocks.css" type="text/css"/>
    <link rel="stylesheet" href="../../static/css/navbar.css" type="text/css"/>
    <link rel="stylesheet" href="../../static/css/home_style.css" type="text/css"/>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Goldman:wght@400;700&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com"/>
    <link href="https://fonts.googleapis.com/css2?family=Goldman:wght@400;700&display=swap" rel="stylesheet"/>
    <link rel="preconnect" href="https://fonts.gstatic.com"/>
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet"/>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300&display=swap" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.js"></script>
    <script src="../../static/js/like.js"></script>
    <script>

        function renderPost(posts, place) {
            let innerHtml = '';

            for (let i = 0; i < posts.length; i++) {
                innerHtml += '<div class="row">'
                innerHtml +=    '<div class="col-12">'
                innerHtml +=        '<div class="main-post">'
                innerHtml +=            '<div class="main-post__head">'
                innerHtml +=                '<h3>' + posts[i].title + '</h3>'
                innerHtml +=            '</div>'
                innerHtml +=             '<div class="main-post__middle">'
                innerHtml +=                 '<p>' + posts[i].text + '</p>'
                innerHtml +=             '</div>'
                innerHtml +=             '<div class="main-post__foot">'
                innerHtml +=                 '<div class="left">'
                innerHtml +=                     '<button type="button" class="btn">'
                innerHtml +=                         '<svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-heart" fill="currentColor" xmlns="http://www.w3.org/2000/svg">'
                innerHtml +=                             '<path fill-rule="evenodd" d="M8 2.748l-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>'
                innerHtml +=                         '</svg>'
                innerHtml +=                     '</button>'
                innerHtml +=                     '<button type="submit" class="btn">'
                innerHtml +=                         '<svg width="1.5em" height="1.7em" viewBox="0 0 16 16" class="bi bi-chat" fill="currentColor" xmlns="http://www.w3.org/2000/svg">'
                innerHtml +=                             '<path fill-rule="evenodd" d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z"/>'
                innerHtml +=                         '</svg>'
                innerHtml +=                     '</button>'
                innerHtml +=                     '<button type="button" class="btn">'
                innerHtml +=                         '<svg width="1.5em" height="1.4em" viewBox="0 0 16 16" class="bi bi-bookmark" fill="currentColor" xmlns="http://www.w3.org/2000/svg">'
                innerHtml +=                             '<path fill-rule="evenodd" d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>'
                innerHtml +=                         '</svg>'
                innerHtml +=                     '</button>'
                innerHtml +=                 '</div>'
                innerHtml +=                 '<div class="right">'
                innerHtml +=                     '<form method="get" action="/post">'
                innerHtml +=                         '<input type="hidden" name="post_id" value="' + posts[i].id + '">'
                innerHtml +=                         '<button type="submit" class="button"><span>Читать </span></button>'
                innerHtml +=                     '</form>'
                innerHtml +=                 '</div>'
                innerHtml +=             '</div>'
                innerHtml +=         '</div>'
                innerHtml +=     '</div>'
                innerHtml += '</div>'
            }

            place.html(innerHtml);
        }

        function sendText(text){
            let data = {
                "text" : text
            };
            $.ajax(
                {
                    type: "POST",
                    url: "/home",
                    data: JSON.stringify(data),
                    success: function (response) {
                        renderPost(response, $('#postTable'))
                    },
                    dataType: "json",
                    contentType: "application/json"
                }
            );
        }
    </script>
</head>
<body style="background-color: rgba(0, 0, 0, 0.7);">
<header>
    <div class="nav transparent navbar-inverse navbar-fixed-top">
        <nav class="navbar-inner navbar-expand-md sticky">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" style="padding-left: 0; margin-left: 0" href="/home">
                        <span>It Park</span>
                    </a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="navbar-left">
                    <div class="chip">
                        <a href="/profile">
                            <img src="../../static/images/logo.jpg" alt="Person" width="96" height="96">
                        </a>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <div class="search">
                                <input id="search" class="form-control mr-sm-2" oninput="sendText($('#search').val())" type="text" placeholder="Search">
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<main>
    <div class="container">
        <div class="row">
            <div class="col-2">
            </div>
            <div class="col-9">
                <div class="row">
                    <div class="col-12">
                        <h1>Posts feed</h1>
                    </div>
                </div>
                <div class="row">
                    <div id="postTable" class="col-12">
                        <c:forEach items="${posts}" var="post">
                            <div class="row">
                                <div class="col-12">
                                    <div class="main-post">
                                        <div class="main-post__head">
                                            <h3>${post.title}</h3>
                                        </div>
                                        <div class="main-post__middle">
                                            <p>${post.text}</p>
                                        </div>
                                        <div class="main-post__foot">
                                            <div class="left">
                                                <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-heart" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M8 2.748l-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                                </svg>
                                                <svg width="1.5em" height="1.7em" viewBox="0 0 16 16" class="bi bi-chat" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z"/>
                                                </svg>
                                                <svg width="1.5em" height="1.4em" viewBox="0 0 16 16" class="bi bi-bookmark" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
                                                </svg>
                                            </div>
                                            <div class="right">
                                                <button type="button" class="button"><span>${post.postCreator}</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
            <div class="col-2">

            </div>
        </div>
    </div>
</main>
</body>
</html>
