<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="my.park.dto.UserDto" %><%--
  Created by IntelliJ IDEA.
  User: Artem Work
  Date: 13.11.2020
  Time: 23:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <title>VOLKSWAGEN.RU</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../static/css/profile.css" type="text/css"/>
    <link rel="stylesheet" href="../../static/css/post_style.css" type="text/css"/>
    <link rel="stylesheet" href="../../static/css/buttons.css" type="text/css"/>
    <link rel="stylesheet" href="../../static/css/blocks.css" type="text/css"/>
    <link rel="stylesheet" href="../../static/css/navbar.css" type="text/css"/>
    <link rel="icon" href="../../static/images/logo.jpg" type="image/x-icon"/>
    <link rel="shortcut icon" href="../../static/images/logo.jpg" type="image/x-icon"/>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Goldman:wght@400;700&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.js"></script>
    <script src="../../static/js/home.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.js"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.js"></script>
</head>
<body style="background-color: rgba(0, 0, 0, 0.7)">
<header>
    <div class="nav transparent navbar-inverse navbar-fixed-top">
        <nav class="navbar-inner navbar-expand-md sticky">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" style="padding-left: 0; margin-left: 0" href="/home">
                        <span>It Park</span>
                    </a>
                </div>
                <div class="navbar-left">
                    <div class="chip">
                        <a href="/profile">
                            <img src="../../static/images/logo.jpg" alt="Person" width="96" height="96">
                        </a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>
<main role="main">
    <div class="container">
        <div class="row">
            <div class="col-2">

            </div>
            <div class="col-8">
                <div class="row">
                    <div class="col-12">
                        <h1>Create post</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <form method="post" action="/profile/add_post">
                            <div class="main-post">
                                <div class="main-post__head">
                                    <div class="create">
                                        <input type="text" name="title" placeholder="Введите тему поста" required="required">
                                    </div>
                                </div>
                                <div class="main-post__middle">
                                    <div class="create">
                                        <textarea name="text" placeholder="Введите текст поста" required="required"></textarea>
                                    </div>
                                </div>
                                <div class="main-post__foot">
                                    <div class="create">
                                        <input type="submit" value="Add">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
</body>
</html>
