import java.util.Scanner;

public class While {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Write a count of numbers");
		int count = s.nextInt();
		double sum = 0;
		while (count > 0) {
			System.out.println("Write a number");
			sum += s.nextDouble();
			count--;
		}
	}
}