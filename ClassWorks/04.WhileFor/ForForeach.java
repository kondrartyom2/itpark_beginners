public class ForForeach{
	//TODO:
	//Вывести все элементы массива
	public static void main(String[] args) {
		int[] numbers = {14, 88, 9, 3};
		for (int i = 0; i < numbers.length; i++) {
			System.out.println("Индекс элемента: " + i + "\n\tЧисло: " + numbers[i]);
		}

		for (int num : numbers) {
			System.out.println("Число: " + num);	
		}
	}
}