public class RusDogs extends Dogs {
    public RusDogs(String name, Integer age, Breed breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }

    @Override
    public String gav() {
        return "ГАВ ГАВ";
    }

    @Override
    public String voice() {
        return "Nem";
    }

}
