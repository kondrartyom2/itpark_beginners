public class NemDog extends Dogs {

    public NemDog(String name, Integer age, Breed breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }

    @Override
    public String voice() {
        return "Nem";
    }
}
