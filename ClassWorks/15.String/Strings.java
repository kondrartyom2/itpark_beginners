import java.util.Locale;
import java.util.Scanner;

public class Strings {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String firstStr = scanner.nextLine();
        String secondStr = scanner.nextLine();
        System.out.println(sortString(firstStr, secondStr));
    }

    private static String sortString(String firstStr, String secondStr) {
        String firstStrToLow = firstStr.toLowerCase();
        String secondStrToLow = secondStr.toLowerCase();
        if (firstStrToLow.equals(secondStrToLow)) {
            return "Они равны: " + firstStr + " / " + secondStr;
        } else {
            int minLength = Math.min(firstStrToLow.length(), secondStrToLow.length());
            if (firstStrToLow.substring(0, minLength).equals(secondStrToLow.substring(0, minLength))) {
                return (minLength == firstStrToLow.length()) ?
                        "1 - " + firstStr + "\n2 - " + secondStr : "1 - " + secondStr + "\n2 - " + firstStr;
            } else {
                for (int i = 0; i < minLength; i++) {
                    if (firstStrToLow.charAt(i) == secondStrToLow.charAt(i)) {
                        continue;
                    } else if (firstStrToLow.charAt(i) < secondStrToLow.charAt(i)) {
                        return "1 - " + firstStr + "\n2 - " + secondStr;
                    } else {
                        return "1 - " + secondStr + "\n2 - " + firstStr;
                    }
                }
                return "Они равны: " + firstStr + " / " + secondStr;
            }
        }
    }
}
