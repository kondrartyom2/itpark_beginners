import java.util.Scanner;

public class Arrays {
    public static void main(String[] args) {
        //TODO: Получить элменты инт вывести наименьший
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество элементов");
        int size = scanner.nextInt();
        int[] array = new int[size];
        for (int i = 0; i < array.length; i++) {
            System.out.println("Введите элемент: ");
            array[i] = scanner.nextInt();
        }

        int min = array[0];
        for (int x : array) {
            if (x < min) {
                min = x;
            }
        }

        System.out.println("Минимальный элеменет: " + min);

        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[i]) {
                    int x = array[j];
                    array[j] = array[i];
                    array[i] = x;
                }
            }
        }

        for (int x : array) {
            System.out.println(x);
        }
    }
}
