import java.util.Date;
import java.util.Scanner;

public class CW2 {
    public static void main(String[] args) {
        // Примитивы
        // целые
        byte x = 3; // 1 байт
//        short y = 3; // 2 байта
//        int k = 3; // 4 байта
//        long t = 3L; // 8 байт
        //вещественные
//        double d = 3.5;
//        float f = 5.7f;
        // символьные
        //char c = 'П';
        // логический
        //boolean flag = true;
        //flag = false;

        //Ссылочные
        //Date date = new Date(System.currentTimeMillis());

        // Арифметика
        //k = 3
//        System.out.println(k + 3);
//        k++ / k += 1 / k -= 1 / k--; - прибавление 1 к "к" - эквивалентно операции k = k + 1
//        System.out.println(k - 3);
//        System.out.println(k * 2);
//        System.out.println(k / 2);
//        System.out.println(d % 2); - остаток от деления
//        System.out.println(k == 3); - равно ли (ответ true или false)
//        System.out.println(k != 2); - не равно ли (ответ true или false)
//        System.out.println((k % 2) == 0); - проверка на четность (ответ true или false)
//        System.out.println(k > 2);
//        System.out.println(k < 2);
//        System.out.println(k <= 2);
//        System.out.println(k >= 2);
        // 2 до 5
//        System.out.println(k >= 2 & k <= 5); это и - не ленивый - проверит оба условия
//        System.out.println(k >= 2 && k <= 5); это и - ленивый - проверит первое условие - если не правда, то дальше не пойдет
        //k > 2 или k < -3
//        System.out.println(k > 2 | k < -3); это или - не ленивый - проверит все условия
//        System.out.println(k > 2 || k < -3); это или - ленивый - проверит до первого совпадения
        Scanner s = new Scanner(System.in);
        System.out.println("Введите свое имя");
        String name = s.nextLine();
        System.out.println("Привет " + name + x + 2);
//        System.out.println("Привет " + name + (x + 2));
    }
}
