import java.util.Scanner;

public class Calculator {
	public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Введите выражение");
        String str = s.nextLine();
        int num1 = Integer.parseInt(str.split(" ")[0]);
        int num2 = Integer.parseInt(str.split(" ")[2]);
        String sign = str.split(" ")[1];
        switch (sign) {
            case ("+"):
                System.out.println(num1 + num2);
                break;
            case ("*") :
                System.out.println(num1 * num2);
                break;
            case ("/") :
                System.out.println(num1 / num2);
                break;
            case ("-") :
                System.out.println(num1 - num2);
                break;
        }
    }
}