import java.util.ArrayList;
import java.util.List;

public class Static {
    public static void main(String[] args) {
        Dog rusDog = new RusDog(Color.BLACK);
        Dog engDog = new EnglDog(12, Color.GREEN);
        System.out.println(rusDog.gav());
        System.out.println(engDog.gav());
    }
}
