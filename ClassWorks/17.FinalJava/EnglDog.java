public class EnglDog implements Dog {

    public Color color;
    public Integer age;

    public EnglDog(Integer age, Color color) {
        this.color = color;
        this.age = age;
    }

    @Override
    public String gav() {
        return "Engl GAV";
    }
}
