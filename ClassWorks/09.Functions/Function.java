import java.util.Scanner;

public class Function {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Добро пожаловать!");
        System.out.println("Введите размер массива: ");
        int[] array = getArr(sc.nextInt(), sc);
        System.out.println("Введите целое чило");
        System.out.println(plusTwo(sc.nextInt()));
        System.out.println("Введите дробное чило");
        System.out.println(plusTwo(sc.nextDouble()));
        plusTwo(array);
        System.out.println(toString(array));
    }

    public static int plusTwo(int x) {
        return x + 2;
    }

    public static double plusTwo(double x) {
        return x + 2;
    }

    public static void plusTwo(int[] x) {
        for (int i = 0; i < x.length; i++) {
            x[i] += 2;
        }
    }

    public static int[] getArr(int arrSize, Scanner sc) {
        int[] array = new int[arrSize];
        for (int i = 0; i < array.length; i++) {
            System.out.println("Введите элемент массива: ");
            array[i] = sc.nextInt();
        }
        return array;
    }

    public static String toString(int[] array) {
        StringBuilder total = new StringBuilder("(");
        for (int j : array) {
            total.append(j).append(", ");
        }
        total.append(")");
        return total.toString( );
    }

    public static int arrSum(int[] array) {
        int sum = array[0];
        for (int i = 1; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }
}
