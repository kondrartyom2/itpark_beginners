import java.nio.file.Path;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regulars {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("(\\+7|8)[0-9]{10}"); // регулярка по телефону в формате +79600872203 или 89600872203
        Pattern pattern1 = Pattern.compile("((\\+7|8)(843))?([0-9]{3}-?[0-9]{2}-?[0-9]{2})"); //
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку");
        if (Pattern.matches("(\\+7|8)[0-9]{10}", scanner.nextLine())) {
            System.out.println(true);
        }
    }
}
